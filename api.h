/*
 * api.h
 *
 *  Created on: Jan 18, 2018
 *      Author: Paweł Wójcicki
 *
 * Copyright (c) 2018 Paweł Wójcicki
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See LICENSE for details.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * TODO: Add rad unit implementation. (for now it's only dummy option)
 * TODO: Change from static linkage to dynamic linkage for unit conversion and boundaries checking.
 * 	 Unit class?
 * 	 Non-linear conversion function possibility? Now it's linear function conversions only (map usage)!
 * TODO: Add velocity, acceleration. Separate logic for linear and non-linear characteristics.
 */

#ifndef _SERVO_API_H_
#define _SERVO_API_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "mbb/hsm.h"
#include "time_event/api.h"
#include "gpio_lib.h"

#ifdef __cplusplus
 extern "C" {
#endif


struct Servod; /* Forward-cast structure for Opaque pointer */
struct Servo;
struct Servo_isr_item;

enum servod_status
{
  SERVOD_STATUS_UNINIT = 0,
  SERVOD_STATUS_STOP,
  SERVOD_STATUS_RUN,
  SERVOD_STATUS_OVERRUN
};


struct servo_configuration
{
  uint8_t id_num;
  const char* name;
  struct gpio_pin_ctl pin;

  uint32_t min_us;
  uint32_t max_us;
  int32_t min_deg;
  int32_t max_deg;
};

struct servod_configuration
{
  const char* name;

  uint8_t hw_timer_num;
  uint8_t ccr_num;
  tev_dispatcher tevd;
  uint32_t period_us;
  uint32_t sort_in_loop_if_remain_us;
  uint32_t sort_timeout_us; /*!< Must by smaller than period_us. */
  uint32_t max_raw;

  mhsm_hsm_t* event_listener;
};


enum {
  SERVOD_NAME_MAX_LENGTH = (16 + 1),
  SERVO_NAME_SIZE = (16 + 1)
};

/* SERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US it's minimal time of servos phase shift.
 *
 * Note:
 *   For adjust to target MCU create variable in makefile before including Rules.mk
 *   ex. CONFIG_SERVO_LIB_IRQ_EXECUTABLE_TIME_IN_US=30
 **/
#ifndef SERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US
#  define  SERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US 15 /* default value */
#endif
enum
{
  SERVOD_IRQ_EXECUTABLE_TIME_IN_US = SERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US,
  SERVOD_IRQ_MAX_PHASE_SHIFT_TIME_IN_US = 100
};

#if defined(__x86_64__)
#  define SERVOD_BASE_SIZE (232 + SERVOD_NAME_MAX_LENGTH)
#  define SERVO_STRUCT_SIZE (35U + SERVO_NAME_SIZE)
#  define SERVO_ISR_STRUCT_SIZE (12U)
#elif defined(__i386__)

#else

#endif

/*
 * Macros set to creating static instance of Servo dispatcher.
 * It's strongly recommended to use: SERVOD_CREATE_STATIC_INSTANCE in .c files.
 * To propagate Servod instance please use: EXTERN_SERVOD_STATIC_INSTANCE
 **/
#define SERVOD_CREATE_STATIC_INSTANCE(NAME, NO_SERVOS) \
static uint8_t _servod_fixmem_## NAME[SERVOD_BASE_SIZE \
				       + SERVO_STRUCT_SIZE * NO_SERVOS \
				       + SERVO_ISR_STRUCT_SIZE * NO_SERVOS * 2]; \
\
int32_t \
init_servod_## NAME (struct servod_configuration* config) \
{ \
  return servod_init ((struct Servod* )_servod_fixmem_## NAME, NO_SERVOS, config); \
} \
struct Servod* const servod_## NAME = (struct Servod* )_servod_fixmem_## NAME

/* EXTERN_SERVOD_STATIC_INSTANCE
 * Instance must be extern for access from application.
 **/
#define EXTERN_SERVOD_STATIC_INSTANCE(NAME) \
extern struct Servod* const servod_## NAME; \
int32_t init_servod_## NAME (struct servod_configuration* config)


void servod_set_phase_shift (struct Servod* const servod, uint16_t phase_shift_in_us);
uint16_t servod_get_phase_shift (struct Servod* const servod);

int32_t servod_set_servo_position (struct Servod* const servod, uint32_t id_num, int16_t deg);
int32_t servod_set_servo_time_us (struct Servod* const servod, uint32_t id_num, int16_t us_10);
int32_t servod_set_servo_raw (struct Servod* const servod, uint32_t id_num, int16_t raw_value);

int32_t servod_run (struct Servod* servod);
int32_t servod_halt (struct Servod* servod);
int32_t servod_stop (struct Servod* servod);

enum servod_status servod_get_status (struct Servod* const servod);
int32_t servod_enable_overrun (struct Servod* const servod, bool new_state);
int32_t servod_attach_servo (struct Servod* const servod, struct servo_configuration* const config);
int32_t servod_deattach_servo (struct Servod* const servod, uint32_t servo_id);

void servod_isr_handler (struct Servod* const servod);
void servod_poll (struct Servod* const servod);
struct Servod* servod_create_dispatcher_instance (uint32_t no_max_servos, void* (*allocator)(size_t));
int32_t servod_init (struct Servod* const servod, size_t no_servos, struct servod_configuration* config);

#ifdef __cplusplus
}
#endif

#endif /* _SERVO_API_H_ */
