SERVO = servo_lib

ifneq ($(CONFIG_SERVO_LIB_IRQ_EXECUTABLE_TIME_IN_US), "")
CXX_DEFS += -DSERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US=$(CONFIG_SERVO_LIB_IRQ_EXECUTABLE_TIME_IN_US)
C_DEFS += -DSERVOD_DEFAULT_IRQ_EXECUTABLE_TIME_IN_US=$(CONFIG_SERVO_LIB_IRQ_EXECUTABLE_TIME_IN_US)
endif

# wildcard for C source files (all files with C_EXT extension found in current
# folder and SRCS_DIRS folders will be compiled and linked)
CSRC += $(wildcard $(patsubst %, %/*.c, $(SERVO)/detail))      
