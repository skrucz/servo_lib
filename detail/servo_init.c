/*
 * servo.c
 *
 *  Created on: Feb 3, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#include "xitoa.h"
#include "cmsis_os.h"
#include "common.h"
#include "debug_log.h"

#include "servo_lib/api.h"
#include "servo_lib/detail/internal.h"


struct Servod*
servod_create_dispatcher_instance (uint32_t no_max_servos, void* (*allocator)(size_t))
{
  assert_param(allocator);

  if (no_max_servos == 0) {
    assert_param(0);
    return NULL;
  }

  /* Allocate space for struct Servod with ISR queue and shadow buffer */
  struct Servod* new_servod = allocator(sizeof(struct _Servod) \
				      + sizeof(struct Servo) * no_max_servos \
				      + sizeof(struct Servo_isr_item) * no_max_servos * 2);
  if (new_servod == NULL) {
    DEBUGSTR_LOG(3, "SERVOD: Not enough memory to allocate Servo dispatcher!");
    return NULL;
  }

  new_servod->status = SERVOD_STATUS_UNINIT;
  return new_servod;
}

int32_t
servod_init (struct Servod* const servod, size_t no_servos, struct servod_configuration* config)
{
  assert_param(servod);
  assert_param(config);
  assert_param(config->tevd);
  assert_param(no_servos != 0);

  xmemset(servod, '\0', sizeof(struct Servod));
  xmemset(&((struct _Servod* )servod)->first_servo, '\0', (sizeof(struct Servo) * no_servos + sizeof(struct Servo_isr_item) * no_servos * 2));

  servod->hw_timer_num = config->hw_timer_num;
  servod->ccr_num = config->ccr_num;
  servod->isr_items = (struct Servo_isr_item* )((uint8_t* )(&((struct _Servod* )servod)->first_servo) + (sizeof(struct Servo) * no_servos));
  servod->isr_items_shadow = (struct Servo_isr_item* )((uint8_t* )servod->isr_items + (sizeof(struct Servo_isr_item) * no_servos));
  servod->curr_isr_item = servod->isr_items;
  servod->phase_shift_val = SERVOD_IRQ_EXECUTABLE_TIME_IN_US;

  xstrncpy(&servod->name[0], config->name, SERVOD_NAME_MAX_LENGTH);
  servod->no_slots = no_servos;
/* TODO: Check configuration parameters correctness. */
  servod->event_listener = config->event_listener;
  servod->period_in_us = config->period_us;
  servod->max_raw = config->max_raw;

  servod->raw_scale.in_lo = 0;
  servod->raw_scale.in_hi = servod->period_in_us * 10; /* Input resolution is: 0.1us */
  servod->raw_scale.out_lo = 0;
  servod->raw_scale.out_hi = servod->max_raw;

  servod->is_overrun_enabled = false;
  servod->is_phase_shifting = true;

  _init_hsm_core(servod, config->tevd);
  servod->status = SERVOD_STATUS_STOP;
  return 0;
}
