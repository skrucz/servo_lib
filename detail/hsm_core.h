/*
 * hsm.h
 *
 *  Created on: Feb 25, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SERVO_DETAIL_HSM_H_
#define _SERVO_DETAIL_HSM_H_

#include "servo_lib/api.h"

#define NO_HSM_SERVOD_TIMOUTS (uint32_t)(4)

enum servo_event_type {
  SERVOD_EVENT_TIMEOUT_SYNCHRO_ON = MHSM_EVENT_CUSTOM,
  SERVOD_EVENT_TIMEOUT_PWM_SET,
  SERVOD_EVENT_ATTACHED_SERVO,
  SERVOD_EVENT_DEATTACHED_SERVO,
  SERVOD_EVENT_SERVO_VAL_CHANGED,
  SERVOD_EVENT_SORT_FINISHED,
  SERVOD_EVENT_ISR_REFRESHED,
  SERVOD_EVENT_OTHER_EVENT
};


void _init_hsm_core (struct Servod* const servod, tev_dispatcher const tevd);

#endif /* EXTRA_LIBS_SERVO_DETAIL_HSM_H_ */
