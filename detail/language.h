/*
 * language.h
 *
 *  Created on: Apr 15, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _DETAIL_LANGUAGE_H_
#define _DETAIL_LANGUAGE_H_

enum txt_servod_errors
{
  TXT_ERR_SERVOD_UNINIT = 0,
  TXT_ERR_SERVOD_WRONG_ID,
  TXT_ERR_SERVOD_ISR_ITEMS_UNINIT,
  TXT_ERR_SERVOD_SERVO_ATTACHED
};

enum txt_servo_errors
{
  TXT_ERR_SERVO_NOT_FOUND = 0,
  TXT_ERR_SERVO_TIME_RANGE_ORDER,
  TXT_ERR_SERVO_DEG_RANGE_ORDER,
  TXT_ERR_SERVO_DEG_RANGE_OUT_OF_SCALE,
  TXT_ERR_SERVO_RAW_RANGE,
  TXT_ERR_SERVO_TIME_RANGE,
  TXT_ERR_SERVO_POSITION_RANGE,
  TXT_ERR_SERVO_SCALE_CALCULATION,
  TXT_ERR_SERVO_DEFAULT_NAME
};


extern const char *txt_eng_servod_errors[];
extern const char *txt_eng_servo_errors[];

#endif /* _DETAIL_LANGUAGE_H_ */
