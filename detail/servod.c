/*
 * servo.c
 *
 *  Created on: Feb 25, 2018
 *      Author: Paweł Wójcicki
 *
 * Copyright (c) 2018 Paweł Wójcicki
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See LICENSE for details.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#include "common.h"
#include "xitoa.h"

#include "servo_lib/api.h"
#include "servo_lib/detail/internal.h"
#include "servo_lib/detail/utils.h"
#include "servo_lib/detail/hsm_core.h"


void
servod_set_phase_shift (struct Servod* const servod, uint16_t phase_shift_in_us)
{
  assert_param(servod);
  assert_param(phase_shift_in_us > SERVOD_IRQ_EXECUTABLE_TIME_IN_US);
  assert_param(phase_shift_in_us < SERVOD_IRQ_MAX_PHASE_SHIFT_TIME_IN_US);

  servod->phase_shift_val = phase_shift_in_us;
}

inline __attribute__((always_inline)) uint16_t
servod_get_phase_shift (struct Servod* const servod)
{
  assert_param(servod);
  return servod->phase_shift_val;
}

int32_t
servod_set_servo_position (struct Servod* const servod, uint32_t id_num, int16_t deg)
{
  assert_param(servod);

  struct Servo* sel_servo;
  if ((sel_servo = get_servo_by_id(servod, id_num)) == NULL) {
    return -1;
  }

  uint16_t new_value;
  if (map_encode_s16_safe((int16_t* )&new_value, deg, sel_servo->deg_to_raw) != 0) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_POSITION_RANGE]);
    return -2;
  }

  sel_servo->value = new_value;
  mhsm_dispatch_event(&servod->hsm, SERVOD_EVENT_SERVO_VAL_CHANGED);
  return 0;
}

int32_t
servod_set_servo_time_us (struct Servod* const servod, uint32_t id_num, int16_t us_10)
{
  assert_param(servod);

  struct Servo* sel_servo;
  if ((sel_servo = get_servo_by_id(servod, id_num)) == NULL) {
    return -1;
  }

  if (is_value_in_servo_time_range(sel_servo, us_10) != true) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_TIME_RANGE]);
    return -2;
  }

  /* Could be unsafe encode because boundaries was checked earlier. */
  sel_servo->value = MAP_ENCODE(us_10, servod->raw_scale);
  mhsm_dispatch_event(&servod->hsm, SERVOD_EVENT_SERVO_VAL_CHANGED);
  return 0;
}

int32_t
servod_set_servo_raw (struct Servod* const servod, uint32_t id_num, int16_t raw_value)
{
  assert_param(servod);

  struct Servo* sel_servo;
  if ((sel_servo = get_servo_by_id(servod, id_num)) == NULL) {
    return -1;
  }

  if (is_value_in_servo_raw_range(sel_servo, raw_value) != true) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_RAW_RANGE]);
    return -2;
  }

  sel_servo->value = raw_value;
  mhsm_dispatch_event(&servod->hsm, SERVOD_EVENT_SERVO_VAL_CHANGED);
  return 0;
}


int32_t
servod_run (struct Servod* servod)
{
  assert_param(servod);
  servod->curr_isr_item = servod->isr_items;

  if ((servod->curr_isr_item)->cmp_val == 0) { /* Don't start if IRQ CCR items not prepared. */
    DEBUGSTR_LOG(3, txt_eng_servod_errors[TXT_ERR_SERVOD_ISR_ITEMS_UNINIT]);
    return -1;
  }

  TIMER_SET_CCR(servod->hw_timer_num, servod->ccr_num, 1);
  TIMER_ENABLE_CCR(servod->hw_timer_num, servod->ccr_num);
  return 0;
}

int32_t
servod_halt (struct Servod* servod)
{
  assert_param(servod);

  TIMER_DISABLE_CCR(servod->hw_timer_num, servod->ccr_num);

  return 0;
}

int32_t
servod_stop (struct Servod* servod)
{
  assert_param(servod);

  servod->curr_isr_item = servod->isr_items;
  TIMER_DISABLE_CCR(servod->hw_timer_num, servod->ccr_num);

  return 0;
}

enum servod_status
servod_get_status(struct Servod* const servod)
{
  assert_param(servod);

  return servod->status;
}

int32_t servod_enable_overrun(struct Servod* servod, bool new_state)
{
  assert_param(servod);

  servod->is_overrun_enabled = new_state;
  return 0;
}

void
servod_isr_handler (struct Servod* const servod)
{
  if (servod->is_phase_shifting) {
    GPIO_SET_MASK((servod->curr_isr_item)->out_mask);
    TIMER_INCREMENT_CCR(servod->hw_timer_num, servod->ccr_num, servod->phase_shift_val);
    servod->curr_isr_item++;
  }else {
    GPIO_CLR_MASK((servod->curr_isr_item)->out_mask);
    servod->curr_isr_item++;
    TIMER_INCREMENT_CCR(servod->hw_timer_num, servod->ccr_num, servod->curr_isr_item->cmp_val);
  }

  if (servod->curr_isr_item->cmp_val == 0) {
    servod->is_phase_shifting = !servod->is_phase_shifting;
    servod->curr_isr_item = servod->isr_items;
    if (servod->is_phase_shifting == true) {
      servod->is_servos_updated = true;
    }
  }
}

/*
 * Attach servo to Servo dispatcher.
 *
 * Successfully added servo is announcement by event to Servod HSM.
 *
 */
int32_t
servod_attach_servo (struct Servod* const servod, struct servo_configuration* const config)
{
  assert_param(servod);
  DEBUGLOG_LOG(3, "Attaching Servo %d...", config->id_num);

  if (is_servod_config_correct(servod, config) != 0) {
    return -1;
  }

  if (get_servo_by_id(servod, config->id_num) != NULL) {
    DEBUGSTR_LOG(3, txt_eng_servod_errors[TXT_ERR_SERVOD_SERVO_ATTACHED]);
    return -1;
  }

  struct Servo* sel_servo = &(&((struct _Servod* )servod)->first_servo)[config->id_num];

  if (config->name) {
    xstrncpy(&sel_servo->name[0], config->name, SERVO_NAME_SIZE);
  }else {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_DEFAULT_NAME]);
    const char default_name[] = { "servo " };

    xstrncpy(&sel_servo->name[0], default_name, SERVO_NAME_SIZE);
    xitoa(config->id_num, &sel_servo->name[sizeof(default_name) - 1], 10);
  }

  xmemcpy(&sel_servo->out, &config->pin, sizeof(struct gpio_pin_ctl));
  sel_servo->min_us = config->min_us * 10;
  sel_servo->max_us = config->max_us * 10;

  /* Convert min/max time [us] boundaries to min/max RAW ovalue for timer CCR. */
  sel_servo->deg_to_raw.in_lo = sel_servo->min_us; /* FIXME: Looks like shit. */
  sel_servo->deg_to_raw.in_hi = sel_servo->max_us; /* FIXME: It's because first is estiminate time to raw range.*/
  if (calculate_servo_scale(servod, sel_servo) != 0) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_SCALE_CALCULATION]);
    return -1;
  }
  sel_servo->deg_to_raw.in_lo = config->min_deg; /* After conversion time to RAW input value could be change do degrees. */
  sel_servo->deg_to_raw.in_hi = config->max_deg;

  mhsm_dispatch_event(&servod->hsm, SERVOD_EVENT_ATTACHED_SERVO);
  DEBUGSTR_LOG(3, "Servo attached successful!");
  return 0;
}

int32_t
servod_deattach_servo (struct Servod* const servod, uint32_t servo_id)
{
  assert_param(servod);
  assert_param(servo_id < servod->no_slots); /* ID is used as indexer for choosing element of array. */
  DEBUGLOG_LOG(3, "Deattaching Servo %d...", servo_id);

  struct Servo* sel_servo;

  if ((sel_servo = get_servo_by_id(servod, servo_id)) == NULL) {
    return -1;
  }

  xmemset(sel_servo, '\0', sizeof(struct Servo));
  mhsm_dispatch_event(&servod->hsm, SERVOD_EVENT_DEATTACHED_SERVO);
  DEBUGSTR_LOG(3, "Servo deattached successful!");
  return 0;
}

