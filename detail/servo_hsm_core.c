/*
 * servo_hsm.c
 *
 *  Created on: Feb 25, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "servo_lib/api.h"
#include "servo_lib/detail/internal.h"

#include "mbb/timer_periodic.h"


MHSM_DEFINE_STATE(operational, NULL);
MHSM_DEFINE_STATE(attach_servo, NULL);
MHSM_DEFINE_STATE(change_servo_val, NULL);
MHSM_DEFINE_STATE(sort_isr_items, NULL);
MHSM_DEFINE_STATE(timer_overrun, NULL);


mhsm_state_t*
operational_fun (mhsm_hsm_t* hsm, mhsm_event_t event)
{
  UNUSED(hsm);

  switch (event.id) {
    case MHSM_EVENT_INITIAL :
    {
    }
    break;
  }

  return &operational;
}

mhsm_state_t*
attach_servo_fun (mhsm_hsm_t* hsm, mhsm_event_t event)
{
  UNUSED(hsm);
  UNUSED(event);

  return &attach_servo;
}

mhsm_state_t*
change_servo_val_fun (mhsm_hsm_t* hsm, mhsm_event_t event)
{
  UNUSED(hsm);
  UNUSED(event);

  return &change_servo_val;
}

mhsm_state_t*
sort_isr_items_fun (mhsm_hsm_t* hsm, mhsm_event_t event)
{
  UNUSED(hsm);
  UNUSED(event);

  return &sort_isr_items;
}

mhsm_state_t*
timer_overrun_fun (mhsm_hsm_t* hsm, mhsm_event_t event)
{
  UNUSED(hsm);
  UNUSED(event);

  return &timer_overrun;
}


static void _hsm_timers_refresh (struct Servod* const servod);

void
servod_poll (struct Servod* const servod)
{
  switch (servod->state) {
    case SERVOD_STATE_SORT :
    {
      mhsm_dispatch_event(&servod->hsm, MHSM_EVENT_DO);
    }
    break;
    case SERVOD_STATE_CUSTOM_LOGIC :
    {
      mhsm_dispatch_event(servod->event_listener, MHSM_EVENT_DO);
    }
    break;
    case SERVOD_STATE_IDLE :
    default:
    {
      /* Do nothing if lack of custom logic and nothing else to do. */
    }
    break;
  };
}

/* HSM periodic timers refresh. Called 1 time per 1/10th of servod period. */
static void
_hsm_timers_refresh (struct Servod* const servod)
{
  mtmr_prd_increment_timers(&servod->hsm, MTMR_NROF_TIMERS(SERVOD_EVENT_TIMEOUT_PWM_SET), *TEV_GET_PTR_2_INTERVAL(servod->tev));
}

void
_init_hsm_core (struct Servod* const servod, tev_dispatcher const tevd)
{
  tev_event new_tev_servod = tev_create_event(tevd, (tev_hndl )_hsm_timers_refresh, servod, servod->period_in_us / 10);
  assert_param(new_tev_servod);
  tev_register_event(tevd, new_tev_servod, 0);
  servod->tev = new_tev_servod;

  mhsm_initialise(&servod->hsm, servod, &operational);
  if (mtmr_prd_initialise_timers(&servod->hsm, MTMR_NROF_TIMERS(SERVOD_EVENT_TIMEOUT_PWM_SET)) != 0) {
    assert_param(0);
  }
  mhsm_dispatch_event(&servod->hsm, MHSM_EVENT_INITIAL);
}
