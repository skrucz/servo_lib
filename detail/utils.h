/*
 * utils.h
 *
 *  Created on: Feb 26, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _SERVO_DETAIL_UTILS_H_
#define _SERVO_DETAIL_UTILS_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "xitoa.h"

#include "servo_lib/api.h"
#include "servo_lib/detail/internal.h"

/* TODO: Add utilities functions for conversion units to RAW CCR value. */
bool is_deg_unit (int16_t unit);
int32_t convert_rad_to_deg (int16_t rad, int16_t* ret_deg);
int32_t calculate_servo_scale (struct Servod* const servod, struct Servo* const servo);
bool is_value_in_servo_time_range (struct Servo* servo, uint16_t us_10);
bool is_value_in_servo_raw_range (struct Servo* servo, uint16_t value);

struct Servo* get_servo_by_id (struct Servod* servod, uint32_t id_num);
int32_t is_servo_id_in_range(struct Servod* servod, uint32_t id_num);
bool is_servo_attached (struct Servo* sel_servo);

int32_t is_servod_config_correct (struct Servod* const servod, struct servo_configuration* const config);

#endif /* _SERVO_DETAIL_UTILS_H_ */
