/*
 * dispatcher.h
 *
 *  Created on: Mar 14, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _SERVO_DETAIL_DISPATCHER_H_
#define _SERVO_DETAIL_DISPATCHER_H_

#include "common.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "mbb/hsm.h"
#include "mbb/timer_periodic.h"
#include "gpio_lib.h"

#include "servo_lib/detail/language.h"
#include "servo_lib/detail/hsm_core.h"


enum servod_state
{
  SERVOD_STATE_IDLE = 0x00,
  SERVOD_STATE_RUN,
  SERVOD_STATE_STOP,
  SERVOD_STATE_HALT,
  SERVOD_STATE_RESUME,
  SERVOD_STATE_ATTACH_SERVO,
  SERVOD_STATE_CHANGE_SERVO_VAL,
  SERVOD_STATE_SORT,
  SERVOD_STATE_CUSTOM_LOGIC
};

struct Servo_isr_item
{
  uint32_t cmp_val;
  struct gpio_pin_ctl out_mask;
};

struct Servo
{
  char name[SERVO_NAME_SIZE];
  uint16_t value;
  struct gpio_pin_ctl out;

  uint32_t min_us;
  uint32_t max_us;
  map16 deg_to_raw;
};

struct Servod
{
  /*
   * mtmr functions assume that mhsm_context(hsm) points to an array of
   * mtmr_prd_t. So this array must be the first component of the state
   * structure.
   * If the timeout events are defined first (see above)
   * MTMR_NROF_TIMERS(last_timer_event) gives the number of timers.
   */
  mtmr_prd_t timers[NO_HSM_SERVOD_TIMOUTS];

  char name[SERVO_NAME_SIZE];
  mhsm_hsm_t hsm;
  mhsm_hsm_t* event_listener;

  bool is_overrun_enabled; /*!< false - algorithm included to TIM IRQ is not executable until
					array of IRQ_events it sorted.
					true - IRQ works with old BUFFER.
					By default is false.  */
  uint8_t no_slots; /* Number of all elements */
  uint8_t hw_timer_num;
  uint8_t ccr_num;
  struct Servo_isr_item* isr_items;
  struct Servo_isr_item* isr_items_shadow;

  volatile struct Servo_isr_item* curr_isr_item;
  volatile bool is_servos_updated;
  volatile bool is_phase_shifting;
  volatile uint16_t phase_shift_val;

  uint32_t period_in_us; /* 1/f Usually: 20ms (50Hz) */
  uint32_t max_raw; /* period / raw_range = raw_resolution */
  map16 raw_scale;
  tev_event tev;

  enum servod_state state;
  enum servod_status status;
};

struct _Servod
{
  struct Servod super;
  struct Servo first_servo;
};

//STATIC_ASSERT(SERVOD_BASE_SIZE == sizeof(struct Servod), "Different size: struct Servod!");
STATIC_ASSERT(SERVO_STRUCT_SIZE == sizeof(struct Servo), "Different size: struct Servo!");

#endif /* _SERVO_DETAIL_DISPATCHER_H_ */
