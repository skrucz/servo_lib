/*
 * servo_utils.c
 *
 *  Created on: Feb 26, 2018
 *      Author: Paweł Wójcicki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "servo_lib/detail/utils.h"


inline bool
is_deg_unit (int16_t unit)
{
  return ((unit >= -360) && (unit <= 360));
}

int32_t
convert_rad_to_deg (int16_t rad, int16_t* ret_deg)
{
  int32_t retval = 0;

  if ((rad >= 0) && (rad <= (2 * 314))) {
    *ret_deg = rad * (1800 / 314); /* FIXME: It's WRONG code! Decide how to handle floating numbers later! */
  }else {
    retval = -1;
  }

  return retval;
}

inline int32_t
calculate_servo_scale (struct Servod* const servod, struct Servo* const servo)
{
  if (map_encode_s16_safe((int16_t* )&servo->deg_to_raw.out_lo, servo->deg_to_raw.in_lo, servod->raw_scale) != 0) {
    return -1;
  }else if (map_encode_s16_safe((int16_t* )&servo->deg_to_raw.out_hi, servo->deg_to_raw.in_hi, servod->raw_scale) != 0) {
    return -1;
  }

  return 0;
}

inline bool
is_value_in_servo_time_range (struct Servo* servo, uint16_t us_10)
{
  return ((us_10 >= (int )servo->min_us) && (us_10 <= (int )servo->max_us));
}

inline bool
is_value_in_servo_raw_range (struct Servo* servo, uint16_t value)
{
  return ((value >= servo->deg_to_raw.out_lo) && (value <= servo->deg_to_raw.out_hi));
}

inline struct Servo*
get_servo_by_id (struct Servod* servod, uint32_t id_num)
{
  if (is_servo_id_in_range(servod, id_num) != 0) {
    return NULL;
  }

  struct Servo* sel_servo = &(&((struct _Servod* )servod)->first_servo)[id_num];
  if (is_servo_attached(sel_servo) == false) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_NOT_FOUND]);
    return NULL;
  }

  return sel_servo;
}

inline bool
is_servo_attached (struct Servo* sel_servo)
{
  return sel_servo->name[0] != '\0';
}

inline int32_t
is_servo_id_in_range(struct Servod* servod, uint32_t id_num)
{
  if (id_num >= servod->no_slots) {
    DEBUGSTR_LOG(3, txt_eng_servod_errors[TXT_ERR_SERVOD_WRONG_ID]);
    return -1;
  }
  return 0;
}

int32_t
is_servod_config_correct(struct Servod* const servod, struct servo_configuration* const config)
{
  int32_t retval = 0;

  if (servod->status == SERVOD_STATUS_UNINIT) {
    DEBUGSTR_LOG(3, txt_eng_servod_errors[TXT_ERR_SERVOD_UNINIT]);
    retval = -1;
  }else if (is_servo_id_in_range(servod, config->id_num) != 0) {
    retval = -2;
  }else if (config->min_us >= config->max_us) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_TIME_RANGE_ORDER]);
    retval = -3;
  }else if (config->min_deg >= config->max_deg) { /* FIXME: Accept reversed characteristics? */
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_DEG_RANGE_ORDER]);
    retval = -4;
  }else if ((!is_deg_unit(config->min_deg)) || (!is_deg_unit(config->max_deg))) {
    DEBUGSTR_LOG(3, txt_eng_servo_errors[TXT_ERR_SERVO_DEG_RANGE_OUT_OF_SCALE]);
    retval = -5;
  }

  return retval;
}
